import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PostsService } from 'src/app/services/posts.service';
import { UtilsService } from 'src/app/services/utils.service';
import { UsersService } from 'src/app/services/users.service';
import { startWith, map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent implements OnInit {
  createPostForm: FormGroup;
  filteredUsers: Observable<User[]>;
  users: Array<User>;

  constructor(
    private postService: PostsService,
    private utils: UtilsService,
    private usersService: UsersService
  ) {
    this.createPostForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      body: new FormControl('', [
        Validators.required,
        Validators.maxLength(100)
      ]),
      user: new FormControl(null, [Validators.required])
    });

    this.usersService.getUsers().subscribe(list => {
      this.users = list;

      this.filteredUsers = this.createPostForm.controls.user.valueChanges.pipe(
        startWith(''),
        map(value => this.utils.filterUsers(value, this.users))
      );
    });
  }

  ngOnInit() {}

  createPost() {
    if (this.createPostForm.invalid) {
      return;
    }

    const formData = this.createPostForm.value;
    if (formData.user && !formData.user.id) {
      this.utils.showError('Please select a valid user');
      return;
    }

    this.postService
      .addPost(formData.title, formData.body, formData.user.id)
      .then(post => {
        this.utils.showSuccess('Post created.');
      })
      .catch(() => {
        this.utils.showSuccess('Please try again.');
      });
  }

  displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }
}
