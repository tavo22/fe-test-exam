import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  hidePassword = true;
  url: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private utils: UtilsService
  ) {
    this.url = this.route.snapshot.params.url;
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.maxLength(50)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ])
    });

    this.loginForm.patchValue({
      email: 'test@exam.com',
      password: '12345678'
    });
  }

  logIn() {
    if (this.loginForm.invalid) {
      return;
    }

    const formData = this.loginForm.value;
    this.authService
      .logIn(formData.email, formData.password)
      .then(() => {
        this.router.navigate([this.url ? this.url : '']);
      })
      .catch(() => {
        this.utils.showSuccess('Cannot login.');
      });
  }
}
