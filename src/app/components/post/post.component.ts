import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from 'src/app/models/post';
import { PostsService } from 'src/app/services/posts.service';
import { CommentsService } from 'src/app/services/comments.service';
import { Comment } from 'src/app/models/comment';
import { UsersService } from 'src/app/services/users.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  postId: number;
  post: Post;
  creatorName: string;
  comments: Array<Comment>;
  showComments = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService: PostsService,
    private commentsService: CommentsService,
    private usersService: UsersService,
    private utils: UtilsService
  ) {
    this.postId = this.route.snapshot.params.id;
    if (this.postId) {
      this.postService
        .getPost(this.postId)
        .then(post => {
          this.post = post;

          this.usersService.getUser(this.post.userId).then(user => {
            this.creatorName = user.name;
          });
        })
        .catch(error => {
          this.utils.showSuccess('Cannot load the post.');
        });
    }
  }

  ngOnInit() {}

  deletePost() {
    this.postService
      .deletePost(this.post)
      .then(() => {
        this.utils.showSuccess('Post deleted.');
        this.router.navigate(['']);
      })
      .catch(error => {
        this.utils.showSuccess('Please try again.');
      });
  }

  viewComments() {
    if (this.showComments) {
      this.showComments = false;
      return;
    }
    this.showComments = true;
    this.commentsService
      .loadComments(this.postId)
      .then(comments => {
        this.comments = comments;
      })
      .catch(error => {
        this.utils.showSuccess('Cannot load the comments.');
      });
  }
}
