import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title: string;

  constructor(private route: ActivatedRoute) {
    route.data.subscribe(data => {
      this.title = data.title ? '> ' + data.title : '';
    });
  }

  ngOnInit() {}
}
