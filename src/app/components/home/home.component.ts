import { Component, OnInit } from '@angular/core';
import { PostsService } from 'src/app/services/posts.service';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { UsersService } from 'src/app/services/users.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  posts: Array<Post>;
  users: Array<User>;
  userFilter = new FormControl();
  userIdFilter: number;
  filteredUsers: Observable<User[]>;

  constructor(
    private postsService: PostsService,
    private router: Router,
    private usersService: UsersService,
    private utils: UtilsService
  ) {
    this.postsService.getPosts().subscribe(list => (this.posts = list));
    this.usersService.getUsers().subscribe(list => {
      this.users = list;

      this.filteredUsers = this.userFilter.valueChanges.pipe(
        startWith(''),
        map(value => this.utils.filterUsers(value, this.users))
      );

      this.userFilter.valueChanges.subscribe(value => {
        this.userIdFilter = value ? value.id : value;
      });
    });
  }

  ngOnInit() {}

  displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }

  deletePost(post) {
    this.postsService
      .deletePost(post)
      .then(() => {
        this.utils.showSuccess('Post deleted.');
        this.router.navigate(['']);
      })
      .catch(error => {
        this.utils.showSuccess('Please try again.');
      });
  }

  addPost() {
    this.router.navigate(['post']);
  }

  viewPost(post) {
    this.router.navigate(['post/' + post.id]);
  }
}
