import { Pipe, PipeTransform } from '@angular/core';
import { Post } from '../models/post';

@Pipe({
  name: 'filterPost'
})
export class FilterPostPipe implements PipeTransform {
  transform(posts: Post[], userId: number): any {
    if (!posts || !userId) {
      return posts;
    }

    return posts.filter(post => {
      return post.userId === userId;
    });
  }
}
