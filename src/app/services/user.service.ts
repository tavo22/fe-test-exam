import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient) {}

  getUser(userId) {
    return this.http
      .get(environment.url + '/users/' + userId)
      .toPromise()
      .then(result => {
        const user = new User();
        Object.assign(user, result);
        return user;
      })
      .catch(error => {
        return error;
      });
  }
}
