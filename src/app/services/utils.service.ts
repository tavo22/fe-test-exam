import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor(private snackBar: MatSnackBar) {}

  public showError(message: string) {
    this.snackBar.open(message, null, {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

  public showSuccess(message: string) {
    this.snackBar.open(message, null, {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  public filterUsers(value: User, users: Array<User>): User[] {
    const filterValue = value && value.name ? value.name.toLowerCase() : '';

    return users.filter(
      user => user.name.toLowerCase().indexOf(filterValue) === 0
    );
  }
}
