import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private users = new BehaviorSubject<Array<User>>([]);

  constructor(private http: HttpClient) {
    this.http
      .get(environment.url + '/users')
      .toPromise()
      .then((result: Array<User>) => {
        this.users.next(result);
      })
      .catch(error => {
        this.users.next([]);
      });
  }

  getUsers() {
    return this.users;
  }

  getUser(userId) {
    return this.http
      .get(environment.url + '/users/' + userId)
      .toPromise()
      .then(result => {
        const user = new User();
        Object.assign(user, result);
        return user;
      })
      .catch(error => {
        return error;
      });
  }
}
