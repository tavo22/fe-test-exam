import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient) {}

  getUser() {
    return this.user;
  }

  isLoggedIn() {
    return this.user.value != null;
  }

  logIn(username: string, password: string): Promise<void> {
    return this.http
      .get(environment.url + '/users/1')
      .toPromise()
      .then(result => {
        const loggedInUser = new User();
        Object.assign(loggedInUser, result);
        this.user.next(loggedInUser);
      })
      .catch(error => {
        this.user.next(null);
      });
  }
}
