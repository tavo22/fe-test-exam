import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Post } from '../models/post';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  posts = new BehaviorSubject<Array<Post>>([]);

  constructor(private http: HttpClient, private router: Router) {
    this.http
      .get(environment.url + '/posts')
      .toPromise()
      .then((result: Array<Post>) => {
        this.posts.next(result);
      })
      .catch(error => {
        this.posts.next([]);
      });
  }

  getPosts() {
    return this.posts;
  }

  getPost(id) {
    return this.http
      .get(environment.url + '/posts/' + id)
      .toPromise()
      .then((result: Post) => {
        return result;
      })
      .catch(error => {
        return error;
      });
  }

  deletePost(post) {
    return this.http
      .delete(environment.url + '/posts/' + post.id)
      .toPromise()
      .then(result => {
        const posts = this.posts.value;
        posts.splice(posts.indexOf(post), 1);
        this.posts.next(posts);
      })
      .catch(error => {
        return error;
      });
  }

  addPost(title, body, userId) {
    return this.http
      .post(environment.url + '/posts/', {
        title,
        body,
        userId
      })
      .toPromise()
      .then((result: Post) => {
        const posts = this.posts.value;
        posts.push(result);
        this.posts.next(posts);
        this.router.navigate(['']);
      })
      .catch(error => {
        return error;
      });
  }
}
