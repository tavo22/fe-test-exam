import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  constructor(private http: HttpClient) {}

  loadComments(postId) {
    return this.http
      .get(environment.url + '/comments', {
        params: {
          postId
        }
      })
      .toPromise()
      .then(result => {
        return result;
      })
      .catch(error => {
        console.log(error);
        return error;
      });
  }
}
